#!/bin/bash

HOST=${1}

ping -c 1 ${1}

STATUS_CODE=$?

if [ "${STATUS_CODE}" = 0 ]
then
    echo "Success"
elif [ "${STATUS_CODE}" = 1 ]
then
    echo "Failed"
elif [ "${STATUS_CODE}" = 2 ]
then
    echo "The transmission was successful but no responses were received."
else
    echo "Unknown error"
fi