#!/bin/bash

read -p "Enter y or n: " ANSWER
case "$ANSWER" in
    [yY]|[yY][eE][sS])
        echo "You choose yes."
     ;;
    [nN]|[nN][oO])
        echo "You answered no."
    ;;
    *)
        echo "Invalid choice"; exit 1
    ;;
esac