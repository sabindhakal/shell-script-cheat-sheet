#!/bin/bash

function backup_file() {
    if [ -f $1 ] # cheeck $1 is a file and it exists
    then
        BACK="/tmp/$(basename ${1}).$(date +%F).$$"  # get basename of the passed in file add the current date and PID of the sehll-script
        echo "Backing up $1 to ${BACK}"
        cp $1 $BACK
    fi
}

backup_file /etc/hosts
if [ $? -eq 0 ]
then
    echo "Backup complete!"
fi