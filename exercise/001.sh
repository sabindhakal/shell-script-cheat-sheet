#!/bin/bash

CONTENT="Shell scripting is fun!"
HOSTNAME=$(hostname)
echo ${CONTENT}
echo ${HOSTNAME}

if [ -e /etc/shadow ]
then
    echo "Shadow password are enabled"
fi

if [ -w /etc/passwd ]
then
    echo "You can edit passwd file"
else
    echo "You can't edit passwd file"
fi

for ANIMAL in "man" "bear" "pig" "dog" "cat"
do
    echo ${ANIMAL}
done

F_NAME=${1}

if [ -d "${F_NAME}" ]
then
    echo "${F_NAME} is a directory!"
elif [ -f "${F_NAME}" ]
then
    echo "${F_NAME} is a regular file!"
else
    echo "${F_NAME} is other type!"
fi