# Shell script

## Shebang

#!/bin/bash

echo "This script uses bash as interpreter"

------

#!/bin/ksh

echo "This script uses ksh as interpreter"
 
## Starting a process in background

Returns a PID

`./script.sh &`

`[1] 43235`

`ps -fp 43235`

## Variables

* Can contain digits, letter, and under_scores
* Can't stat with digit
* Lower case is also allowed, however it is best-practice to use upper-case.
* 

### Define a variable
`VARIABLE_NAME="Value"`

### Use a variable
`$VARIABLE_NAME` or `${VARIABLE_NAME}`

The curly braces syntax is used to pre or follow variable name with additional data. Example 

`!#/bin/bash`

`MY_SHELL="bash"`

`echo "I am ${MY_SHELL}ing on my Keyboard.`

### Adding command output to a variable
You can assign the output of a command in a variable with the following syntax

`!#/bin/bash`

`SERVER_NAME=$(hostname)`

`echo "You are running the script on ${SERVER_NAME}"`

You can also achieve this using ``SERVER_NAME=`hostname` `` , however this is considered old.

## Tests
Tests are used to make decisions. Test can be done for sever conditions like:
* Check if the strings are equal
* If the file exists
* If a number is greater than other
* e.t.c

### Syntax

`[condition-for-test]`

Example

`[-e /etc/passwd]`
If the file exists it returns true (exists with status 0). If it does not exist it returns false. (exists with status 1)

param | function
---|---|
-dFILE| True if file is a directory
-eFILE| TRUE if file exists
-fFILE| TRUE if file exists and is a regular file
-rFILE| TRUE if file is readable by you
-sFILE| TRUE if file exists and not emply
-wFILE| TRUE if file is writable by you
-xFILE| TRUE if file is executable by you
-zSTRING| TRUE if the string is empty
-nSTRING| TRUE if the string is not empty
STRING1=STRING2| TRUE if STRING1 is equal to STRING2
STRING1!=STRING2| TRUE if STRING1 is not equal to STRING2

### Arithmetic Operators

operators | function
---|---|
arg1 -eq arg2 | TRUE if arg1 is equal to arg2
arg1 -ne arg2 | TRUE if arg1 is not equal to arg2
arg1 -lt arg2 | TRUE if arg1 is less than to arg2
arg1 -le arg2 | TRUE if arg1 is less than or equal to arg2
arg1 -gt arg2 | TRUE if arg1 is greater arg2
arg1 -ge arg2 | TRUE if arg1 is greater than or equal to arg2

## Making decisions

### If statement

#### Syntax
`if [condition-is-true]`

`then`

* `command1`

`elif [condition-to-be-true]`

* `command2`
  
`else`

* `command1`

`fi`

---
Example

`#!/bin/bash`

`MY_SHELL="bash"`

`if ["$MY_SHELL" = "bash"]`

`then`

* `echo "You seem to like bash"`

`fi`

## For loop

### Syntax
`for VARIABLE_NAME in ITEM_1 ITEM_N`

`do`

* `command1`

* `command2`

* `commandN`

`done`

### Example

`#!/bin/bash`

`for COLOR in red green blue`

`do`

* `"COLOR: $COLOR"`

`done`

`# Renaming picture in a folder`

`#!/bin/bash`

`PICTURES=$(ls *jpg)`

`DATE=$(date +%F)`

`for PICTURE in PICTURES`

`do`

* `echo "Renaming ${PICTURE} to ${DATE}-${PICTURE}"`
* `mv ${PICTURE} ${DATE}-${PICTURE}`

`done`

## Positional parameters

$ script.sh parameter1 parameter2 parameter3

* $0: "script.sh"
* $1: "parameter1"
* $2: "parameter2"
* $3: "parameter3"

### Example

`#!/bin/bash`

`echo "Executing script: ${0}"`

`echo "Archiving user: $1"`

#Lock the account

`passwd -1 $1`

#Create an archive of the home dir.

`tar cf /archives/${1}.tar.gz /home/#{1}`

`done`

---

## Exit Statuses
* Every time a command is executed it returns an exit status.
* Exit status range from 0 - 255
* 0 --> success
* Exit status other than 0  is error 

Shell scripts can also return exit status. They can be configured explicitly
* exit 0
* exit 1
* exit 2
* exit 255

If you don't specify the exit status in your script, it will use the exit status of the previously executed command.

Check exist status

`$?` : contains the return code of the previously executed command

Exit status can be assigned to a variable and use later
`RETURN_CODE=$?`

Example:

`ls /sample/location`

`echo "$?"`

--

`HOST="google.com"`

`ping -c 1 $HOST`

`if [ "$?" -ne 0]`

`then`

* `echo "$HOST unreachable!`

`fi`

### Logical AND and OR

#### && = AND
Commands can be chained together using the && operator. Command following the && wil only be executed if the previous command executes successfully.

`mkdir /tmp/bak && cp test.txt /tmp/bak`

#### || = OR

`cp test.txt /tmp/bak || cp test.txt /tmp`

In the example above if one of the cp command exits with non-zero exit status status the other one will run. One one of the cp will run.

### ; (Semicolon)
Separate commands with a semicolon to ensure they all get executed.

`cp test.txt /tmp/bak/ ; cp test.txt /tmp`

Same as

`cp test.txt /tmp/bak/`

`cp test.txt /tmp`

---

## Functions

Definition

`function function-name() {`

* ` #code`

`}`

OR 

`function-name() {`

* ` #code`

`}`

Call the function like this

`function-name`

The function keyword is not required. Calling a function just put the name of the function you want to be called in a new line without braces

Functions need to be declared before in the code before they can be used! It is a best practice to place all your functions on the top of your scripts.

* You can access the parameter passed to the functions using $1, $2 ..
* $@ contains all the  parameters
* $0 contains the script itself, not the function name

Example

`#!/bin/bash`

`function hello() {`

`echo "Hello $1"`

`}`

`hello James`

#Outputs: Hello James

---
## Variable / Function Scope

* By default, variables are global
* Vars. have to be defined before they can be used

If a global var. is defined within a function it is not available outside until the function is called

`#!/bin/bash`

`function init_function() {`

* `GLOBAL_VAR=1`

`}`

#GLOBAL_VAR is not available yet

`echo $GLOBAL_VAR`

`init_function`

#GLOBAL_VAR is now available

`echo $GLOBAL_VAR`

### Local variable

* Can only be accessed within a function
* Defined using local keyword
    * local LOCAL_VAR=1
* Only functions can have local var

$$ get the PID of the currently running shell script

## Checklist for a good shell script

1. Shebang
2. Comments/file header
3. Global variables
4. Functions
    1. use local variables
5. Main script content
6. Exit with an exit status
    1. `exit <status>`. If no exit status is defined last executed command's exit status will be used.

## Wildcards

`ls *.txt` : List all files with .txt

`ls a*`: List all files starting with a

`ls ?`: List all the files that are 1 char long

`ls ??`: List all the files that are 2 char long

* Character class

`ls c[aeiou]t`: List all files / folders that start with c, end with t and contain vowels in between

`ls [a-d]*`: List all files starting with character a,b,c,d

`ls *[[:digit:]]`: List all files with digits 

Example:

`#!/bin/bash`

`cd /var/www`

`for FILE in *.html`
`do`
* `echo "Copying ${FILE}`
* `cp $FILE /var/www-just-html`

`done`

---

## Case statement

Alternative to if / elif / elfi ... else

`case "$VAR" in`

* `    pattern_1)`

    * `        # Command to be executed`

* `     ;;`

* `    pattern_N)`

    * `        # Command to be executed`

* `    ;;`

`esac`

---

## Logging in shell script
 
### Syslog

* The syslog standard uses facilities and severities to categorize messages.
    *   Facilities: kern, user, mail, daemon, auth, local0
    *   Severities: emerg, alert, crit, err, warning, notice, info, debug
* Logfile locations are configurable
    *   /var/log/messages
    *   /var/log/syslog

### Logging with logger
* By default creates user.notice messages

`logger "Message"`

`logger -p local0.info "Message"`

`logger -t myscript -p local0.info "Message"`

`logger -i -t myscript "Message"` # -t Tag a message. -i include the PID

Mac example: https://apple.stackexchange.com/questions/256769/how-to-use-logger-command-on-sierra

---
## Loops

### While loop

`while [ CONDITION_IS_TRUE ]`

`do`

* `command 1`
* `command 2`

`done`

### Infinite loop
Might be handy where you want the script to be running continuously

`while true`

`do`

* `command 1`
* `sleep 1`

`done`

Example:
Loop 5 times

`while [ $INDEX -lt 6 ]`

`do`

* `mkdir /usr/local/project-${INDEX}`
* `((INDEX++))`

`done`

### Return code of command

This check the return code of the ping command and continues till it encounters error.

`while ping -c 1 app1 > /dev/null`

`do`

* `    echo "app1 still up.."`

* `    sleep 5`

`done`

### Reading a file, line-by-line

See exercise  read-file-line-by-line.sh

---
## Debugging

`#!/bin/bash -x` Turn debugging for the whole script

Debugging only a portion of script

...

set -x # start debugging

echo $MY_VAR

set +x # end debugging

...

* -e = Exit on error
* -e can be combined with other options
    * #!/bin/bash -ex